const express = require('express');
const router = express.Router();

const userController = require('../Controllers/userController.js');


// [Routes]

// User Registration Route
router.post("/register", userController.userRegistration);

// User Authentication
router.post("/login", userController.userAuthentication);

// User Details
router.post("/details", userController.getUser);






module.exports = router;