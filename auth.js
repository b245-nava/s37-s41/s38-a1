const jwt = require('jsonwebtoken');
// User-defined string data that will be used to create JSON web tokens
// This is used in the algorithm for encrypting our data, which makes it difficult to decode the information without defined secret keywords.

const secret = 'CourseBookingAPI';

// [Section] JSON web token
	// JSON web token or JWT is a way of securely passing the server to the frontend or the other parts of the server.

	// Information is kept secure through the use of the secret code.
	// Only the system will know the secret code that can decode the encrypted information.

// Token Creation
/*
	Analogy:
		It's like packing the gift/information and providing the secret code, which is the key
*/

	// The argument that will be passed to our parameter(user) will be the document/information of our user.
module.exports.createAccessToken = (user) => {

	// This serves as our payload, which contains the data that will be passed to other parts of our API.
	const data = {
		_id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// .sign(), which comes from the jwt package, will generate a JSON web token
	// Syntax: jwt.sign(payload, secretCode, options);
	return jwt.sign(data, secret, {});

}